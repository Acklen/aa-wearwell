resource "aws_instance" "instance" {
  instance_type = var.instance_type
  ami           = var.ami
  count         = var.instance_count

  tags = {
    Name = local.name_prefix
  }

  key_name               = var.key_name
  vpc_security_group_ids = var.vpc_security_group_ids
  subnet_id              = var.subnet_id
  iam_instance_profile   = aws_iam_instance_profile.s3_profile.name
  ebs_block_device {
    device_name = "/dev/sdg"
    volume_size = 30
    volume_type = "gp2"
  }
}

resource "aws_iam_instance_profile" "s3_profile" {
  name = "s3-instance-profile"
  role = "ec2-s3-role-wearwell"
}