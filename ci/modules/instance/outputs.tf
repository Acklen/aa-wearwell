output "ec2_id" {
  description = "The id of the instance"
  value       = aws_instance.instance.*.id
}

output "arn" {
  description = "The arn of the instance"
  value       = aws_instance.instance.*.arn
}

output "public_ip" {
  description = "The public IP addresses assigned to the instance"
  value       = aws_instance.instance.*.public_ip
}