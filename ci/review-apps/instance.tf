module "instance" {
  source = "../modules/instance"

  project        = var.project
  environment    = var.environment
  instance_count = 1

  ami                    = "ami-040809ffc2a1b2f32"
  instance_type          = "c5.large"
  key_name               = "wearwell-reviewapp"
  vpc_security_group_ids = ["sg-085e74c898a9d45c0"]
  subnet_id              = "subnet-0ba039314091e7d5d"
}