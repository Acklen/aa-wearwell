terraform {
  backend "s3" {
    region = "us-east-1"
  }
}

provider "aws" {
  default_tags {
    tags = {
      Environment = "AA-ReviewApps"
      Devops      = "Diego"
      Company     = "Acklen"
    }
  }
}
